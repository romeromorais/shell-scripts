#!/bin/bash

echo """
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+  _   _ _  __ _   ___           _        _ _                                             +
+ | \ | (_)/ _(_) |_ _|_ __  ___| |_ __ _| | |                                            +
+ |  \| | | |_| |  | || '_ \/ __| __/ _' | | |                                            +
+ | |\  | |  _| |  | || | | \__ \ || (_| | | |                                            +
+ |_| \_|_|_| |_| |___|_| |_|___/\__\__,_|_|_|                                            +
+                                                                                         +
+ instalador autonomo do apache-nifi 1.14.0                                               +
+ desenvolvido por Romerito Morais https://www.linkedin.com/in/romeritomorais/            +
+ testados nas distribuiçõs derivados de RHEL, Debian                                     +
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""

export USER_DIST=$(cat /etc/os-release | head -3 | tail -1 | cut -d "=" -f 2)
export USER_HOME=$HOME
export JAVA_HOME_PATH="/usr/lib/jvm/java-1.8.0/"

export VERSAO="1.14.0"
export APPLICATION="apache-nifi"
export INSTALL_BIN_NIFI="https://dlcdn.apache.org/nifi/${VERSAO}/nifi-${VERSAO}-bin.tar.gz"

sudo rm -rf /opt/nifi-${VERSAO}

echo """
************************************************************************************************************************
instalando as dependencias do ${APPLICATION}-${VERSAO} ...
************************************************************************************************************************
"""

if [ ${USER_DIST} = fedora ] || [ ${USER_DIST} = centos ]; then
    sudo yum update -y
    sudo yum install java java-devel -y
    sudo yum install wget -y
    sudo yum install nano -y
    sudo yum install xdg-utils -y
elif [ ${USER_DIST} = ubuntu ] || [ ${USER_DIST} = linuxmint ]; then
    sudo apt update -y
    sudo apt install java java-devel -y
    sudo apt install wget -y
    sudo apt install nano -y
    sudo apt install xdg-utils -y
fi

clear

echo """
************************************************************************************************************************
instalando o ${APPLICATION}-${VERSAO} ...
************************************************************************************************************************
"""

cd /tmp
sudo wget -c ${INSTALL_BIN_NIFI}
sudo tar -xvf nifi-${VERSAO}-bin.tar.gz
sudo chmod +x nifi-${VERSAO} -R
sudo cp -v -r nifi-${VERSAO} /opt
sudo ln -s /opt/nifi-${VERSAO}/ /usr/local/nifi
sudo /opt/nifi-${VERSAO}/bin/nifi.sh install
sudo rm -v -rf /tmp/nifi-${VERSAO}-bin.tar.gz
sudo rm -v -rf /tmp/nifi-$VERSAO

sudo cp -v ${USER_HOME}/nifi-env.sh /opt/nifi-${VERSAO}/bin
sudo chmod +x /opt/nifi-${VERSAO}/bin/nifi-env.sh
rm -rf ${USER_HOME}/nifi-env.sh

#clear

echo """
************************************************************************************************************************
configurando ${APPLICATION}-${VERSAO} como serviço ...
************************************************************************************************************************
"""
sudo service nifi start
sudo systemctl start nifi


xdg-open https://127.0.0.1:8080/nifi


